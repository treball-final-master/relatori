---
title: 02 MPEG-G Part 1

...

### Part 1
The part 1 is identified by the code [ISO/IEC 23092-1:2019](https://www.iso.org/standard/57795.html) this part is the one focused on defining the document structure for storage and transport. The storage format is used to save the document on the machine or resource server, is a format to define how to access the data  and how to protected. The trasposrt structure is focused on the sending of the document, this structre enable the data streaming.

Focusing on the storage format, as can be seen on the figure \ref{fig:fileformatMPEG} an overview on how it's structured the data on an MPEG-G file. The file is organized on a hierarchical structure composed by the next encapsulation levels: File format, dataset group, dataset, access unit and blocks.


![MPEG-G file format Font: @vogesINTERNATIONALORGANISATIONSTANDARDISATION](../images/mpegg_file_structure.png){width=400px height=300px}

File format is the first box in the hierarchical level, and must be before any variable length box [@InformationTechnologyGenomic].  All the boxes need to follow the hierarchical structure represented on the figure \ref{}, so inside the file format it's allocated the dataset group.

 The dataset group is used to store an amount of datasets of a particular study. But the data it's not set as raw data inside the dataset, it's stored on a layer under the dataset called access unit. The access unit is composed by blocks. These are genomic records, so in the access unit we find an aggregation of them, which will be encoded. This aggregation enables the access and the inspection of the genomic records separately.

The format to transport the document still uses the hierarchical format but with a little variation, as can be seen on the figure below. 

![mpegg_transport_format_overview.png](mpegg_transport_format_overview.png){width=400px height=300px}

The transport mode encapsulates, inside one or more packets, the data inside the MPEG-G file. If we compare the two structures, we see that the transport mode deletes the boxes of the dataset group and dataset.

On the definition of the ISO/IEC standard, it's explicitly recommended the decodification of the transport mode started by the _dataset_mapping_table_list_ to organize the document as in the storage mode.

To read the data correctly, the classes are represented by a KLV (Key length value)  data structure.  And the KLV syntax is defined by the following structure:
```c
struct gen_info
{
    c(4)    Key;
    u(64)   Length;
    u(8)    Value[];
}
```

This data is stored at the first bytes of the file, are the one which tells the reader the content type, the length of the file and the value, The content type is encoded on **key** field, this field is composed by 4 chars. The list of possibles values can be found on the ISO document, the value is used to mark which data will be found on the document, to help the reader understanding which elements stored on the next bytes. 

The **length** is used to write how many bytes can be found on the value, to parse correctly the document and is encoded in little endian.  All the boxes, except the file format, have this variable. 

Finally, can be found the **value**, on this field is where goes the data. The data will be encoded in XML, following the schemas defined by the protocol.