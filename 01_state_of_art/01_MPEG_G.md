---
title: 01 MPEG-G 
...

## MPEG-G 

The MPEG-G protocol is developed by a currently under development standard by a working group of the MPEG (Moving pictures expert group). MPEG is an a working group, which develop standards for media coding [@MovingPictureExperts2021]. 


This standard has been registered under de ISO/IEC 23092. It is a standard focused on the storage of genomic data in a hierarchical structure, which apport some benefits such as  [@vogesINTERNATIONALORGANISATIONSTANDARDISATION]  ** REVISAR PAPER: EL JAIME ENVIARA EL MÉS NOU** :**Selective access**,  the file contains embdedd tools to jump directly to the wanted data, **Data streaming**, as is presented on the next chapter, the standard defines a file structure which enables th epacket sending. This structure permits the receiver stard reading the data before all the file is received. 

On the field of genomics, the standard enables the **genomic studies aggregation**, this means  that amount of related genomic stdies can be stored in the same MPEG-G file. This point is important because permits the execution of querys over the file and get transversal data. The MPE-G also have strong **enformencement of privacy rules**, as is presented on the section of the part 3, the file contains an specific setion where define who can access and which actions can perform over the resource. 

On the encryption point of view, is permited the **selective encryption of sequencing data and metadata**, which defines a way to encrypt the diferent file levels of the hierarchy.

Also many others properties are defined, for example the **interoperability**, this property enables the conversion to other medical data structures as FASTQ, SAM or BAM, the other ones can be consulted on the MPEG-G whitepaper, but are not going to be explained because are not used during the project.

To achieve all the explained properties and so many others, the standard is developed by parts. Those parts are focused on solve one or more than one propierty and adds extra functions to the standard, those parts are:

* Part 1, project structure
* Part 2, codification
* Part 3, Metadata and protection Box
* Part 4, reference software
* Part 5, conformance
* Part 6, 

All the parts except the number 6, have an stable and published version, those version are on revision and continiously improvement. The parts 6 is still on development.