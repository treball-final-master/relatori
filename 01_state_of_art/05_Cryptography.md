---
title: 05 Access Policies
...

# Access Control Policies

Th access control policies are mechanismes to determine if a user is allowed to execute some action over other resource. Exists soo many mechanisms to achive this kind of protection, all of them have a different approach to solve the problem or are used to differents porposes. 

The access control policies can be found on many different areas for exemple in a computer sistem, a user can just access to her files or determinate files which other users shares with him, but the root user have access over all the sistem files. This is a hierarchy based access control sistem. The higher role the user have, more are the acctions that can perform.

There are many access control policies methodologies, the most common ones are( [@huAssessmentAccessControl2006] ):

* **ACL (Access Control List)**, a sistem based on a list of rules associated to an object. The list contains a the users  and the accions permited to specific resource. 
* **RBAC (Role-Based Access Control)**, this access policy approach is not based on rules, is based on roles. Are defines a list of roles, and the acctions enabled to each and the user just can perform determinated action based on the role.
* **RuBAC (Rule-Based Acces Control)**,  an approach focused on restrict or able actions based on a list of rules, and example can be, that to access a resource you must have an IP from catalonia. All the request which not pass this rule are going to be discarted.
* **XACML (eXtensible Access Control Markup language)**, this is a complete standard, which defines how to define de policies and the architecture and how to process the requests, which uses XML files to define access policies, this definition of the policies can be used to define very grained access rules, can conbine rules and roles. For example, just can acces to a file the users with a admin role and are connected with a catalan IP.

The next subchapter talks about XACML, because is the one which is used during the project development, to get a better understanding of it, the file structure, the rules and how it works are going to be explained. 

## XACML

The XACML (eXtensible access control markaup language) protocol is an access control protocol defined to obtaine atomic rules to define very accurate who have access  to execute an acction to a resource.  


The XACML standard defines how to build the full architecture and its components, on the figure (??), is presented a graphical overview of all the components and how they interact each other. 


![font: @huAssessmentAccessControl2006](../images/xacml_structure.png)

The figure presents many components the ones, but not all of them are part of the architecture, the ones which belongs really to the protocol are:

* **PEP (_policy enforcement point_)**, it's the point which will recive the request for access to any resource, and will derivate it to the PDP, to evaluete if the user has permission to perform the action
* **PDP (_policy decision poin_)** , this element is the one that will recive the evaluation request of the PEP, and will return if the final user has permissions or not to perform the action. To evaluate the request it will use two other components, the PAP, to evaluate the stored policies, and the PIP to get extra information about the attributes and evaluate more acuratly. 
* **PAP (_policy administration point_)**  this point will request to the PRP the access rules and will be the one in charge of managing them. During the verification process, when the PDP requires the access rules to be evaluated, the PAP will be the one which will send it to it to be verified.
* **PIP (_policy information point_)**, will manage the achivement of additional atributes like, for example, some enviroment data or aditional resources to return a more accurate response. 
* **PRP (_policy retrival point_)**, this point will be the one which will store the XACML policies. Those policies can be on a database or in a file. On this project those rules will be on the MPEG-G file, insede the protection box.
