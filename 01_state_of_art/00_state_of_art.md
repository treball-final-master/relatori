---
title: 00 State of Art
...
\clearpage
\newpage
# State of art

This chapter reviews the two main standards focused on enabling medical record sharing and collaboration between scientific teams in an encrypted and secure way. The two standards, which will be talked about, are MPEG-G and Crypt4gh. 

MPEG-G is a standard that defines how to encrypt one or more than one medical study in a single file, creating a tree structure grouping the medical data, the study data and the raw data, in an ordered way enabling the random access. 

On the other hand Crypt4gh is more focused to store the raw data of a study, more than one study can be stored on the file, but just the raw data sections one after the other.  

The MPEG-G subchapter will be mainly focused on parts 1 and 3. All the standard has 6 parts, all parts except the part 6 have a first version, the 6 is not published. but will just focus on these parts because it's used during the project development. Part 1 talks about the file structure, and part 3 talks about the security.

On the Crypt4gh side, it will be defined the protocol created and developed by the GA4GH (_Global alliance for genomics and health_). The cypher suite and the document structure will also be explained.