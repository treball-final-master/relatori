---
title: 00 Conclusions
...

# Conclusions and Future work

This chapter will present the conclusions of the project after studying the MPEG-G standard and Crypt4GH, and the implementation of the Software reference, and will define some tasks which can be done in the future to improve the project.

The first, presented objective is the _proposal of insert crypt4gh in MPEG-G_, the document presented purposes a vision where the crypt4gh can be inserted at the end of the access unit level of the MPEG-G standard, as an encryption method of the access unit. This proposal is due to MPEG-G standard enables the classification in layers of a medical study, permitting the navigation between levels to find the wanted data in an ordered way. On the other side, the crypt4gh just encrypts the raw data. At the MPEG-G standard the encrypted raw data is situated on the access level, for that the proposal defines the insertion of the Crypt4gh protocol at the access unit level. As a proposal can't be validated by the author.

The second objective was the _definition of the software reference architecture_, under this objective was developed the software reference, the software can be reviewed in the chapter 4, to validate this objective was defined 3 use cases, as the multiple scenarios where it can be used the MPEG-G protocol. The three use cases were reviewed one by one and all the three cases the software answered correctly, so can be defined as a successful test.

The last one is: _define a guide to collaborate with the project_. The second objective defines the reference software, but to be proposed as the reference software for MPEG-G standard, must be defined a way to use it and improve it. Because the protocol, it on continuous improvement, and still remains the part 6 to be defined. The software reference must be ready to grow at the same time that the standard does it. For the validation of this objective, exists three chapters, those are the _how to install it, how to deploy it and how to improve it_. These three chapters just are the introduction of them, they fully result documents are on the annexes. The result can be checked on the GitLab page deployed in the reference software repository.

As a future line of work will be interesting to focus on the file components, at this point the software work with the part 3, and the part 1, so a future work can be the implementation of the remaining parts of the protocol. At the protocol level the interesting part will be focused on defining services for every part, to compile all the operation which applies the MPEG-G standard.

If we focus on the services presented on the project, a future work, is the increment of the number of rules which can be created with the policy service, due to at that level, we can just add rules to control the access with the email, and add elements such as the department or any other information will increase the granularity of the rules. 

Also a bug revision is required, this software currenty have som little issues during some policies verifications that requires an analisis to find all the bugs and fix them. 



