---
title: 00 Methodology
...
\clearpage
\newpage
# Methodology and Project development

This chapter covers all the aspects related to the methodology and the tools used for the development of the project. It will be defined the general structure of the project and the elements which composes it, and there will be given a justification for the used structure.

It will be explained the used technologies in every part, and it will be shown the role belonging to them in the project. The first explained element will be the authentication service and authorization, where it will be explained the JWT and how it will be used in the project. After defining the first service, the core elements which are the PEP (policy enforcement point) and the PDP (policy decision point) will be explained: the Balana, a framework to implement the XACML protocol, and why they are used in the context of MPEG-G. 

Finally, it will be found the description of the Rest API, talking about the encryption, decryption and protection box services exposed through that Rest API.

## Requirements
    
### Software requirements    

To develop the project were used the next softwares, frameworks and programing lenguages:

* **Node.js** [@node.jsDownload]
* **Postgresql** @groupPostgreSQL2021
* **Java** [@JavaSoftware]
* **Springboot** [@SpringBoot]
* **Docker** @EmpoweringAppDevelopment
* **Postman** [@Postman]
* **Swagger.io** [@APIDocumentationDesign]
* **Gitlab**    [@IterateFasterInnovate] 
 







