---
title: 01 Software architecture
...

## Software architecture
The MPEG-G standard is composed by many parts, and it's still under development. Due to that, proposed architecture is one based on microservices, since this architecture enables the option to add new services on the mesh without touching the existing ones, preventing the addition of bugs on the working features.

The project will be structured in 3 main components: The authentication service, the one which will manage the final user login, which will return a token to manage the session. Before the authentication barrier, the file service will be found. This service will manage the file requests when a user requests the access to a file, the file server will ask to balana to check if the user has the right permissions to read or write the file. If the service returns a true, the file server will send a message to encrypt and decrypt service. The last component is the one which will finally return the file decrypted. If the user doesn't have permissions to access the file server will return a _402_ to block the user and warn him that he doesn't have rights to access the file.


![Sistem overview ](../images/Sistem_overview.png){width=300px height=300px}


The microservices approach is used to enable the growth of the reference software, if some developer wants to increase the functionalities adding new features developed by the working group, it will just need to add the service on the mesh, and tell the file server to send a request to it to add the functionality.

But also it's enabled the change in the approach. If someone wants to be used a different Auth service, just needs to be modified the auth service. The balana is also a separated service, for the same reason, if another team prefers other policy verifier, it just needs to change the balana service.

## Authentication and Authorization

As was defined on the previous subchapter, a component of the service mesh, it's the authentication and authorization service. This one, it's the one, will take care of the user login process and the management of the session. To manage the sessions, an JWT token will be used, to achieve a RESTful API and help the PDP to obtain data related to the user who is sending a request to the server.

The authentication and authorization it's an endpoint which will manage the login and sing up process, the service will manage 3 scenarios:

* Sign up process, the process to create new users. the service will receive a **POST** requests, with the user email and password, and will generate a new entry on the users database. 
* Login process, it's the process to able a user access to the server. 
* Validate sessions, on this scenario, the service will receive a request with a token on the header, and will return if the token it's still valid.

The login endpoint will check if the pass user email and passwords match with any entry of the database, if the data match with any entry will return a JWT token. The token will be used to pass data to the PDP to check if the users have the right permissions to read or write the file, and will control the session. The JWT will encode the next data:

header:
```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```
payload:
```json
{
  "sub": "mpegg_reference_software",
  "email": <user_email>,
  "iat": <timestamp_issue>,
  "exp": <timestamp_issue + 30 days>, 
}
```