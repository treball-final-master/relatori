---
title: 03 Rest Api
...

## RestAPI

A REST (Representational state transfer) API (Application programming interface), Is a collection of architectural parameters which must be followed in a software during her development. The RESTful services enables the creation of web API's using methods defined on the HTTP protocol: GET, POST, PUT, DELETE

![api_overview.png](../images/api_overview.png)


The web APIs are exposed to the world through and URL, and enables an entry point opened to everybody.

To send data to the server, it's used the HTTP packet payload followed by one of the methods defined to insert data: PUT or POST. The main purpose of the RESTful APIs is the increase of the performance, scalability, visibility, portability and resistance of a server.  [@RepresentationalStateTransfer2021] 

This chapter will be focused on the utilization of that architecture to build the desired software.

As is wanted to build a modular architecture to enable the component modification, for example someone prefers other XACML framework, a decoupling between the services is required. Encapsulate all the services in separated services permits this premise.

If the focus is putted on the encryption and decryption service, on figure \ref{fig:fileDecEnc}, can be a schematic representation of how the fileserver take advantage of the HTTP endpoint exposed by the decrypt and encrypt service to work with the data stored on the file server.

![](../images/fileserver_decrypt.png)

If the decrypt and encrypt service is modified, but not changed the HTTP path, the fileserver will never know about the change.

This decoupling approach is used on all the composes, and all the services are exposed through an HTTP endpoint.

On the previous subchapter, was defined the PEP and PDP elements. The PEP, as was defined the element which will receive the request, on the project will be the fileserver, and the balana, as was defined previously, the PDP. To enable the use of other PDP, the balana service will be exposed through an endpoint too.