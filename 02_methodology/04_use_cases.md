---
title: 04 use cases
...

## Use case and RefSW use Enviroment

In the previous subchapter of this chapter, it was defined the methodology used to develop the software, were defined the architecture, the approach, and the internal components. On this one it will be defined how the software is going to be tested, and to do so, there will be defined a series of cases to analyze if the objectives were achieved or not.

The defined cases are the following:  
  
  
* As a covid researcher, I want to enable my college in another university access to the medical study data securely  
* As security researcher, I want to verify the signatures of encrypted documents to prevent impersonations  
* As medical researcher, I want to decrypt and verify documents received from other researcher with no knowledge of cryptography.  
* As a researcher from an external institution collaborating with the local research team, I want to access the data from the team.  
  
 Those use cases would be used to define some scenarios which the reference software must pass to define the objectives as achieved.