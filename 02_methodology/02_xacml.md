---
title: 02 Xacml
...

## Access policy management 

On the previous defined service mesh, it can been seen a component called Balana, it is an XACML open source implementation of the protocol registed under an Apache2 licence. This framework will be the one used to check the access policies.

As it is defined on the state of art chaper, on the XACML  section, the procotol helps on the definition and verification of certain attributes related with the access policy. Helps on the relation between the subject, the resource, the accion and the enviroment. It enables the definition of the policies on a very low level, and it returns the resolution of a access request. [@IAMConceptWeek2017] 

The protocol doesn't just check if a list of attributes has the access to a determinate resource, also enables the creation of them. The system managament of the rules can be published into an external resource, this permits the modification of the rules from an independant point. This rules can be stored in a database or in a file  [@XACML2019]. In the context of the project it will be in a file, because it will come defined in the MPEG-G file, in the box security field.


![ XACML Overview diagram Font: @XACML2021 ](../images/xacml_overview_wikipedia.png){width=300px height=250px}

On the figure \ref{fig:xacmlOverview} it can be seen the flow of a request of a resource over a sistem which implements an access control based on XACML, balana, as is an opensource implementation of the XACML  follows the same structure (figure ??)


![Balana as PDP: oververview](../images/balana_overview.png){width=300px height=250px}

Balana works as the PDP (figure \ref{fig:balanaPDP}), it will be the component over the PEP requests if the user can access to the resource. On the figure \ref{fig:SystemOverview}, it can be seen that the one which will act as PEP will be a file management services, this one, when it receives a read/write request it will ask to balana if the current user has rights to access to the server. 

Once the file service receives the request, this one will generate an a XACML request, which will be sent to the balana. Tha balana will execute the veririfacion process. To do this task it will read the policies from the protecction box of the file, and will get the user data from the token, with this data it will process the request, and return the result to the file server. 

Balana will act as standalone service, it will not be implemented on the same service of the file service, this approach it's because 

