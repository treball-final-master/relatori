---
title: 01 Purpose for integrate Crypt4gh on MPEG-G
...

## Integration of Crypt4gh on MPEG-G 

Crypt4gh it’s a cryptographic standard the Global Alliance for Genomics and Health, for able to exchange of genomic data between researchers securely. [@Crypt4GHSecureMethod]


On the other side we find the MPEG-G protocol, This standard it’s being developed by the large international team under de ISO/IEC 23092. The MPEG-G, It’s focused on able to exchange, store, compress and protect genomic information.

The difference resides in the file structure and the focus of the standard, crypt4gh it’s thought to store a group of genomic records in a serial (figure 2), and MPEG-G (figure 3), it’s a KLV, with a defined hierarchy, we can achieve the encryption on every level. On each level, we get deeper into the data, arriving at the last level, where is the genomic record. This hierarchical level, able the exploration of the data, and permits the researchers to store complete research data on the document.

Figure 2: Crypt4gh file structure

Figure 3: MPEGG file structure

## File comparison
If we compare the standard’s purpose, we find the same objective, share genomic records with a big focus on the security, to prevent data leaks, and achieve the information exchange between researchers. The main difference is in the file structure, and on some additional features. MPEG-G, not just able of the encryption, also adds the option to explore the data and execute some search operation over the file. Due to the hierarchical structure, and the metadata.

### Ciphers
In our context, a good point to compare is the able cipher suite on both files. We are going to talk about the cipher’s that are considered on each standard. Will be compared the cipher suite used for encrypting the genomic records and encrypt the key. MPEG-G, not just able of the encryption, also adds the option to explore the data and execute some search operation over the file. Due to the hierarchical structure, and the metadata.

Genomic Data Encryption, On the MPEG-G side, we find other type of encryption standards, the working group decided to implement the AES standard, using CTR (counter mode) and GCM (Gallois Counter Mode) mode [@InformationTechnologyGenomica]. The key size can be, 128, 192 or 256 bits. For the genomic data encryption, both uses symmetric encryption algorithms, but they use completely different standards. On the side of Crypt4gh it’s used the ChaCha20-IETF-Poly1305 [@GA4GHFileEncryption] They affirm that this algorithm was chosen due to the capability to encrypt larger files than other encryption standards.

The ChaCha20-IETF-Poly1305, variant was chosen because having the support of the IEFT. This support grants better library support. Key Encryption After encrypting the genomic data, both of them encrypts the key to share it with the other researchers. MPEG-G, uses asymmetric and symmetric encryption to store the key. Both of standards able the multi sharing option, this consists in encrypt the key many times with different public keys, to send the file to many researchers or institutions.
Crypt4GH uses asymmetric encryption to encrypt the key and sent it to many. The ciphered key will store on the header, to encrypt the key it’s used Elliptic Curve Diffie-Hellman uses X25519.

The encryption of the key to decrypt the data on the side of MPEG-G it’s permitted with two different standards. One is the RFC 3394, these standards define a way to encrypt the key to decrypt the data with a symmetric encryption over the key with an encryption key to decrypt it. The other permitted standard is PKCS1 OAEP [@kaliskiPKCSRSACryptography1998]

## Proposal
After the comparison of the type of standards, it’s purpose to integrate Crypt4GH inside the MPEG-G standard. The both standard it’s structured differently, MPEG-G, offers an option to organize multiple studies on a single file, and Crypt4GH just able the encryption of genomic records. With this difference the approach of this purpose, it’s insert the Crypt4GH on the access unit level MPEG-G. The access unit layer is the one with the genomic records, so there can be stored the genomic records. The idea builds an XSD adapting the crypt4gh serial structure to an XSD to insert it on the encryption box of MPEG-G. About the difference on the Cipher suits it’s solved just integrating the ciphers of crypt4gh on the MPEG-G standard just for this case.

The modification over the base protection box of an access unit it’s the addition of an attribute on the protection type header, to set the file type, can be Crypt4GH, to advise that the data inside it Crypt4gh standard, or empty to use the standard MPEG-G. The resulting schema can be checked on the annexes.