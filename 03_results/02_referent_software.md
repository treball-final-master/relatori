---
title: 02 Referent Software
...

## Reference Software

In this section will be presented the software developed during the project. The result is a software built over a microservice architecture which exposes an API through an HTTP endpoint. The results will be presented and discussed on the first subchapter. After the presentation and a little tour over it, there will come three subsections which are focused on showing how to install it, how to deploy it and how to improve it. These last three parts will be a little introduction on those topics, but extended on the Annex.

### Overview

The presented software was commented before, is built in over a microservice architecture, on the figure \ref{fig:service_mesh}, it can be seen the service mesh diagram. The final result is composed by the API gateway, the youth service and the file server. The file server at the same time is composed by the encrypt and decrypt service, the protection box service and the balance.

![](../images/service_mesh.drawio.png)

All the components and subcomponents operate as a unique software, and the internal mesh it's hidden to the final user. Now all the components will be deeply explained:  

**API gateway**, this element is the door to the external world. it's exposed by and http endpoint. This element will be the one which will redirect the traffic between the auth service, or the fileserver. Also the api gateway will operate as authentication guard, as all the services need that the user must be logged, every request which goes to the fileserver, first will be redirected to the auth service, which will return if the user is correctly logged or not.

**Authorization and Authentication**, on this service it will be find the users managment. This service will manage the login and sign up process, also it will be controlling the token generation, and the user data checking. The login and sign up will be done just with the email and the password, if the user sends a valid user and password, this service will retun the session token, this token must be inserted on all the requests over the file server due to not existing any public request. Another important taks of this server is the checking of the valid user data, on every request to the file server, the api getway first will redirect the request to this service, this service will take the token and divide and analyse it to check if it's correct, if all it's correct will return a _status 200_ which will advice the api gateway that the request can go to the file server. But if the token it's wrong, will return an _status 401_ which will block the flow and return a error to the user.

**Fileserver**, is the last main component, and it's the main focus of this project. The api gateway it's focused on offering the door to the world and manage the requests, the Authorization and Authentication service will manage the users, and the file server is the one which will manage the files. This server is were it's found the implementation of the MPEG-G protocol, using the subcomponents: Encryptation, Decrypt, Balana and protecction box, will perfome all the operation of the part 3.  The service will receive a valid operation, which will  be processed using the submodules. The **Encryption and Decryption** service, will be the one which will manage the operation of encryptiing and decrypting, this service will receive a request with the mpeg-g file and will perform the wanted accion over it. If the acction is decrypted, first will check the signature searching if it's valid, but on the other hand, if its required and ecryption operation will encrypt the file and sign it.

Other submodule is the **protecction box**, this one will manage the generation and extraction of data of the proteccion box of and MPEG-G file. On this endpoint it can be created the protecction box, it can be deleted or add it new rules. And the last one is the balana, which is the implementation of the balana framework over an a rest api, to open an endpoint to verify if the user has access to the desired resource. The flow of a file server resource, for example,  to read operation over an encrypted file will be (figure \ref{fig:requestExampleFlow}):

1. The request will get into the server though the api gateway, which will will send it to the auth server
2. The auth service will verificate if the token set on the request header is correct and will return a response to the gateway.
3. If the auth service returns a _401_, that means that the user is unathorized, and will return the user a _401_ and close the request. But if a _200_ is returned from the auth service, the gateway will send the request to the fileserver to be processed.
4. The file server will receive the request and before decrypt the file and send it to the user, will check if the user had the rights to perfom the action. So will send to the **protecction box service** a request to extract the policies from the wanted file.
5. The **protecction box service** will receive a MPEG-G file, from which will extract the policies and send it in json format back tho the fileserver service.
6. The fileserver, now will take the user data from the token, the MPEG-G file and the policies, and will send it to the balana.
7. The balana will perfome a verification to check if the user has rights to perform the acction and send the response to the fileserver.
8. Again on the file server if the balana returns a false, the service will return a _401_ to the user, but if a true is returned, will take the password and the file and send it to the decryption service.
9. The decryption service will receive an MPEG-G file and the password. Will take that data and perfom a decryption operation and return the data decrypted.
10. The fileserver will receive de data decrypted and return it to the user.

It also exists another service, which is not presented on the mesh because doesn't perfom any operation, but it's important to be explained because is the one which contains the API documentation. This service can be found under the path `/docs` and it's an static service which will return the list of endpoints, how to use them, and an interactive execution. This documentation is built using the Swagger software. This is a software developed by the open api organitzation focused on defining an standarized way to present Rest api's documentations(figure \ref{fig:swaggerScreenShot}.


The endpoins can be grouped on four blocks, two are the main ones, which will be the ones used on a realy deployment, those are the authentication and authorization endpoints, which belongs to the user operations. And the fileserver block, which are focused on the management of the mpeg-g files, the operations are read and write. Also enables the option to get the file list and the navigation into the files getting into the subgroups of the mpeg-g files.

Finally the other two blocks are defined to test the application, those are the encryption and decryption endpoints and the ones from the protection box. These two blocks enable the communication directly to those endpoints bypassing the system guards, which are defined because during the testing process was required a direct access to those services, and can be useful to understand the underlayer of the app. It will also be useful to test the use cases defined on the previous chapters.

Now will be presented in a summed up way how to install, deploy and improve the software. The next chapters will be just a presentacion and a little introducction on how to do it, the full documentation is on the Annexes, also with the user manual to enable anyone the use of that software.

### How to install it
As was presented on the methodology chapter, the software is built on the top of an microservice achitecture. To wake all the service mesh it was defined and a docker-compose file, which will operate as orchestator and service name resolver. (figure\ref{})

To install the software is required to have installed the docker-compose and git, the annex document has a special chapter to solve the installation of those components.

To obtain the source code must be clone from the next gitlab repository: [MPEG-G Source code](https://gitlab.com/treball-final-master/mpeg-g-reference-software)

![MPEG-G source code Qr](../images/mpeg_g_source_code_qr.png)

Once the code is cloned to the machine, it's time to deploy it, no extra resources are need it, due to all the libraries and programing lenguages are encapsulated inside the docker containers.


### How to deploy it

After the code clone, the next step is to deploy the software. For that it's used the docker-compose engine. The docker-compose is the one which will create a private virtual network inside the machine, to hide the container ip's inside, and enable the network connection between them.

Also docker-compose will set the service name as domain name, this helps the developer enabling the use of the service name to call another api service, and the engine will manage the redirection to the correct service. To wake up the container mesh with docker container a terminal on the cloned project is need it. Once the terminal is ready docker-compose can be call it to start the process:

``` bash
$ docker-compose up
```

That instruction will start the stack. the container will be pulled from their registries, and after that it will be runned. After the waking up process it's done, a browser can be opened and search: _localhost:8080_ a white page or non-found page will be shown, but that's normal due to the root of the domain being empty, but that means that the reverse proxy is working.

### How to improve it

The presented software contains the implementation of the parts 1 and 3. But not it's the completed standard, this is because the protocol also contains other parts, and it's still under development. For this reason the software it's licenced under a open-source project to enable anyone to improve, modify and use it.

But as any open-source project, collaboration rules are defined to improve the quality code, create a wealthy development environment, and keep clean the working tree. Also the definition of collaborating code of conduct is proposed to encorauge the participation on the project defining a transparent decission making process creating a clarifying way on how to participate in the the decission making process.

* To add any improvement a pull request must be done
* Any branch can be merged without at least 1 approval
* To add any new service must be created a new repository and just add the conainter on the main deployment yaml file
