\clearpage
\newpage
# Results

The next chapter is focused on the results part, it's going to present the resulting project achieved using the previously defined methodology. Results will be split into 3 subchapters, The first one will present the purpose to integrate the Crypt4gh protocol inside the MPEG-G standard. 

Then it will be presented the reference software developed, a section focused on giving a tour over the reference software, after that it will be explained how to install it, how to deploy it and how to improve it. Finally, there will be found a section called _Use case validation_, on this chapter it will be executed the software over each use case defined in the methodology chapter.