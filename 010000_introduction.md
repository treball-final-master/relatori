---
title: Introduction
...

\clearpage
\newpage
# Introduction
The genome is all the genetic material of an organism [@Genome2021].  If we focus on the human genetics, it's composed 23 pairs of chromosomes, if we count all of them, we obtain 3200 millions pairs  of DNA (Deoxyribonucleic acid) bases.  
  
Determinate parts of the DNA sequence will be traduced to amino acids that will compose the proteins, and these proteins will execute all the functions of the human organism: Human development, the correct function of the system ... To summarize: the human being.  
  
The genetic code is replicated every time a cell is duplicated. The replication machine is not perfect, and is producing some errors, this is what produces differences between humans, and the evolution. There are some errors, that we take special focus, because they produce a faulty sequence of DNA that is traduced into a dysfunctional protein. And this is interesting because can generate a disease.  
  
The  arrived to a high level of knowledge on the biotechnics field, that enable the full read and reconstruction of the human DNA. We can read it and process it faster than ever. The interesting of that, is since years ago, we can identify a high number of disease, based on mutation on this DNA sequences. This disease creates a disruption on the system that will produce a bad function of the organism.  
  
This data is the most accurate representation of a human, so it's a very critical information and need to be stored and distributed in a safety way. Any leak of this data can suppose a very high privacy issue. Nowadays with the high volume of information we have about the genetic data, not all the scientific team can have all te information and need to be echanged between teams, so we need some protocol to enable the security during the storage and the transmission.  
  
To solve part of this problem were defined two protocols, the MPEG-G and the Crypt4GH. The first one is currently on development by a working group of MPEG,  . The second one is defined by the GA4GH (Global Alliance for genomics and health). MPEG-G defines an standard to , and Crypt4GH is a encryptation Both of them are focused on the encryption of genomic records. But MPEG-G also defines a way to store and encode them.  
  
The first section introduces de the MPEG-G and the Crypt4GH,  Will be presented the both protocols, the file structure which they use, and finally the cipher used in the both cases.  
  
On the second section introduces the methodology used during the development process, presenting all the components, and the architecture used to achieve the objects, and finally some use cases used to test and evaluate the result.  
  
Then, on the third section, will be focused on the achieved results, three subsections compose this chapter, the first will present a proposal to insert the Crypt4GH protocol inside the MPEG-G. Then will be presented the referent software, on this subchapter will be an introduction to the reference software and a tour over it to explain how it works. After that will come a guide to install, deploy and improve it, and finally will be the revision of the use cases defined to test the software. If the reference software pass all the use cases, the project will be defined as a success.  
  
Finally, will come the conclusions, where it can be found a resume of the development process, and an iteration over the proposed objectives reviewed the achieved results. On that chapter also will be defined some future work lines to enable the continuation of the software and the improvement of it and with this, the end of the document.

## Objectives
This chapter presents the objectives that are the main focus of the project:

* Build a reference software of the MPEG-G standard.  
* Define an implement proposal to integrate the crypt4gh protocol inside the MPEG-G protocol
* Define a guide on how to deploy, install and improve the reference software
